import ROOT

### Context manager to be able to open TFiles with the "with" statement and have
### the file automatically closed
from contextlib import contextmanager
@contextmanager
def openTFile(filename, mode = 'READ'):
  tfile = ROOT.TFile(filename, mode)
  try: # We want to close the tfile even if an exception happened
    yield tfile
  finally:
    tfile.Close()


########################################
##### NTuple prefetch and classify #####
########################################
def _addDataToResult(data, result, field, idlist = None, add_ids = False):
  if isinstance(data, list) and '_' in field:
    [cat, sub_field] = field.split('_',1)
    if cat not in result:
      result[cat] = [{'index':idlist[i]} if idlist and add_ids else {} for i in range(len(data))]
    for (sub_data, sub_result) in zip(data, result[cat]):
      _addDataToResult(sub_data, sub_result, sub_field)
  result[field] = data

TClonesArrayParseInt= [
  'seg_hitsExpWire',
  'seg_phiHits_superLayer',
  'seg_phiHits_layer',
  'seg_phiHits_wire',
  'seg_phiHits_side',
  'seg_zHits_layer',
  'seg_zHits_wire',
  'seg_zHits_side',
  'ph2Seg_hitsExpWire',
  'ph2Seg_phiHits_superLayer',
  'ph2Seg_phiHits_layer',
  'ph2Seg_phiHits_wire',
  'ph2Seg_phiHits_side',
  'ph2Seg_zHits_layer',
  'ph2Seg_zHits_wire',
  'ph2Seg_zHits_side',
  'mu_staMu_matchSegIdx',
  ]

def prefetchFields(evt, toFetch, only_ids = None, add_ids = False):
  result = {}
  
  for field in toFetch:
    data = evt.__getattr__(field)
    datatype = str(type(data))
    
    if 'vector' in datatype or 'TClonesArray' in datatype:
      ids_to_get = sorted(list(set(only_ids))) if only_ids else range(len(data))
      if 'vector' in datatype:
        data = [data[i] for i in ids_to_get]
      if 'TClonesArray' in datatype:
        ### Do not touch this code!! TClonesArray behaves very weirdly,
        ### it can't be manipulated with python logical way of doing things
        parser = (lambda x:int(x)) if field in TClonesArrayParseInt else (lambda x:x)
        data = [ [parser(data[i][j]) for j in range(len(data[i]))] for i in ids_to_get ]
    else:
      ids_to_get = None

    _addDataToResult(data, result, field, ids_to_get, add_ids)

  return result

chambers = [(wh,se,st) for wh in range(-2,3) for se in range(1,15) for st in range(1 if se<13 else 4,5) ]
def byWhSeSt(data):
  result = {chamb:[] for chamb in chambers}
  for item in data:
    key = (item['wheel'],item['sector'],item['station'])
    if key not in result: result[key] = []
    result[key] += [item]
  return result