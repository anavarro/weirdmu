#!/bin/env python
import time

import ROOT
ROOT.gROOT.SetBatch(True) # Prevent pop-up plots

from dttree_utils import openTFile

### Parse command-line arguments
def parse_cmd(argv):
  if (len(argv)<2):
    print "usage: rootest.py NtupleFileName [Number of events]"
    raise SystemExit
  args = {}
  args['filename'] = argv[1]
  args['maxevts'] = int(argv[2]) if (len(argv)>2) else -1
  return args

def run(argv, d, process_f, finish_f):
  args = parse_cmd(argv)

  ### Open the NTuple, process it, and fill the plots
  with openTFile(args['filename'], mode='READ') as tfile_in:
    t_last_print = time.time()
    i_entry = -1
    # for some ntuples it's just 'DTTree'
    for entry in tfile_in.Get('dtNtupleProducer/DTTREE'):
      i_entry+=1
      if i_entry == args['maxevts']: break
      if time.time() - t_last_print > 2:
        print("INFO: Processed %i entries in the DTTree"%i_entry)
        t_last_print = time.time()

      ### PROCESSING STARTS HERE
      process_f(entry, d, i_entry)
      
  finish_f(d)

  ### Save the plots as png
  can = ROOT.TCanvas("can", "histograms ", 1000, 1000)
  for key in d.plots:
    d.plots[key].Draw()
    can.Update()
    can.SaveAs('plots/'+key+'.png')

  ### Writing the plots to a root file
  #with openTFile('output.root', mode='RECREATE') as tfile_out:
  #  tfile_out.cd()
  #  for key in d.plots:
  #    d.plots[key].Write()

