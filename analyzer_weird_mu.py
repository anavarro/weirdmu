#!/bin/env python
import sys
from pprint import pprint
from misc_utils import AttrDict, histo_add
from dttree_utils import prefetchFields
from runanalysis import run
import ROOT
ROOT.gROOT.SetBatch(True) # Prevent pop-up plots

# equivalent to 12 hits with 1 mm drift error each
SUM_POS_ERR_SQ_THR = .12


### Define the plots and analysis variables
d = AttrDict({
  'plots' : {
    },
  'histo':{},
  'worst_mu':{'sum_pos_err_sq':0},
  'traveller_mu':{'max_t_diff':0},
  })

def process(entry, d, i_entry):

  nSeg = entry.seg_nSegments
  nMu = entry.mu_nMuons
  
  muons = prefetchFields(entry, [
      'mu_staMu_matchSegIdx',
      'mu_staMu_nMatchSeg',
      ], add_ids = True )['mu']

  nSegInMu = len(set([id for mu in muons for id in mu['staMu_matchSegIdx'][:mu['staMu_nMatchSeg']] ]))
  nMuWithSeg = len([ mu for mu in muons if mu['staMu_nMatchSeg'] ])
  
  histo_add(d.histo, 'nSeg', nSeg)
  histo_add(d.histo, 'nMu', nMu)
  histo_add(d.histo, 'nSegInMu', nSegInMu)
  histo_add(d.histo, 'nMuWithSeg', nMuWithSeg)
  
  for mu in muons:

    if mu['staMu_nMatchSeg'] == 1:

      #######################################################################
      ## Try to guess why some segments are upgraded to "single-segment muon"
      #######################################################################

      tests = []
      tests += [ entry.mu_isGlobal[mu['index']]                           ]
      tests += [ entry.mu_isTracker[mu['index']]                          ]
      tests += [ entry.mu_isRPC[mu['index']]                              ]
      tests += [ entry.mu_isLoose[mu['index']]                            ]
      tests += [ entry.mu_trkMu_numberOfMatchedStations[mu['index']]  > 0 ]
      tests += [ entry.mu_trkMu_numberOfMatchedRPCLayers[mu['index']] > 0 ]
      tests += [ entry.mu_staMu_numberOfValidMuonHits[mu['index']]    > 0 ]
      tests += [ entry.mu_eta[mu['index']] > .85                          ]

      histo_add(d.histo, ['single-segment muons', 'all'] )
      histo_add(d.histo, ['single-segment muons', 'has_something_else', any(tests)] )

    if mu['staMu_nMatchSeg'] > 1:
      
      muSegList = mu['staMu_matchSegIdx'][:mu['staMu_nMatchSeg']]
      
      segs = prefetchFields(entry, [
        'seg_wheel',
        'seg_sector',
        'seg_station',
        'seg_posLoc_x',
        'seg_posLoc_y',
        'seg_dirLoc_x',
        'seg_dirLoc_y',
        'seg_dirLoc_z',
        'seg_hitsExpPosCh',
        'seg_phi_t0',
        'seg_phiHits_posCh',
        'seg_phiHits_side',
        'seg_phiHits_wire',
        'seg_phiHits_layer',
        'seg_phiHits_superLayer',
        'seg_phiHits_time',
        'seg_zHits_posCh',
        'seg_zHits_side',
        'seg_zHits_wire',
        'seg_zHits_layer',
        'seg_zHits_time',
        ], only_ids = muSegList, add_ids = True )['seg']
      
      
      #############################################################
      ## Find muons with more than one segment for the same chamber
      #############################################################
      
      sameChamber = []
      identical = []
      
      for i in range( mu['staMu_nMatchSeg'] ):
        for j in range(i+1, mu['staMu_nMatchSeg'] ):
          if all([ segs[i][key] == segs[j][key] for key in ['wheel','sector','station'] ]):
            sameChamber += [(i,j)]
          if all([ segs[i][key] == segs[j][key] for key in segs[i] if key != 'index' ]):
            identical += [(i,j)]
      
      histo_add(d.histo, [ 'reps', 'sameChamber', mu['staMu_nMatchSeg'], len(sameChamber)] )
      histo_add(d.histo, [ 'reps', 'identical'  , mu['staMu_nMatchSeg'], len(identical  )] )
      if len(sameChamber):  histo_add(d.histo, [ 'reps', 'sameChamber', 'total'] )
      if len(identical):    histo_add(d.histo, [ 'reps', 'identical'  , 'total'] )
      
      
      for (i,j) in sameChamber:
        if (i,j) not in identical:
          differentHits = []
          repeatedHits = 0
          for cat in ['zHits','phiHits']:
            digi_id_fields = ['superLayer', 'layer', 'wire', 'time']
            for s_id in [i,j]:
              for hit in segs[s_id][cat]:
                hit_hash = tuple([2 if (k=='superLayer' and cat=='zHits') else hit[k] for k in digi_id_fields])
                if hit_hash not in differentHits:
                  differentHits += [hit_hash]
                else:
                  repeatedHits +=1
          
          histo_add(d.histo, [ 'reps', 'not identical; by not-repeated-digis num', len(differentHits)-repeatedHits] )
          histo_add(d.histo, [ 'reps', 'not identical; by not-repeated-digis num', 'total'] )
          
      
      ################################################
      ## Find muons with segments whose T0 is very off
      ################################################

      sum_pos_err_sq = 0
      
      for seg in segs:
        for hit in seg['phiHits']:
          sum_pos_err_sq += abs(hit['posCh'] - seg['hitsExpPosCh'][(hit['superLayer']-1)*4+(hit['layer']-1)] )**2
        for hit in seg['zHits']:
          sum_pos_err_sq += abs(hit['posCh'] - seg['hitsExpPosCh'][4+(hit['layer']-1)] )**2
            
      
      if sum_pos_err_sq > SUM_POS_ERR_SQ_THR:
        histo_add(d.histo, [ 'wild_t0_mu' ] )
        histo_add(d.histo, [ 'wild_t0_mu_(Loose,Medium,Tight)', (entry.mu_isLoose[mu['index']], entry.mu_isMedium[mu['index']], entry.mu_isTight[mu['index']]) ] )
        histo_add(d.histo, [ 'wild_t0_mu_(numberOfValidMuonHits>=2)', entry.mu_staMu_numberOfValidMuonHits[mu['index']]>=2 ] )
        histo_add(d.histo, [ 'wild_t0_mu_(normChi2<10)', entry.mu_staMu_normChi2[mu['index']] < 10] )
        histo_add(d.histo, [ 'wild_t0_mu_(isStandalone)', entry.mu_isStandalone[mu['index']]] )
      
      if sum_pos_err_sq > d.worst_mu['sum_pos_err_sq']:
        d.worst_mu['sum_pos_err_sq'] = sum_pos_err_sq
        d.worst_mu['mu'] = mu
        d.worst_mu['phi_t0s'] = [seg['phi_t0'] for seg in segs]
        d.worst_mu['segs'] = segs
      
      
      fitted_t0s = [ seg['phi_t0'] for seg in segs if seg['phi_t0']>-999.0 ]
      max_t_diff = max(fitted_t0s) - min(fitted_t0s) if fitted_t0s else 0
      
      if max_t_diff > d.traveller_mu['max_t_diff']:
        d.traveller_mu['max_t_diff'] = max_t_diff
        d.traveller_mu['mu'] = mu
        d.traveller_mu['phi_t0s'] = [seg['phi_t0'] for seg in segs]
        d.traveller_mu['segs'] = segs
      
      
def finish(d):
  septxt = '#'*80 + '\n'
  print septxt*2
  print 'Report of strange muon features in the NTuple'
  print
  print 'There are %i muons, of which %i have associated DT segments'%(d.histo['nMu'], d.histo['nMuWithSeg'])
  print 'There are %i segments, of which %i belong to a muon, and %i do not'%(d.histo['nSeg'], d.histo['nSegInMu'], d.histo['nSeg']-d.histo['nSegInMu'] )

  if d.histo['single-segment muons']:
    print '\n'+septxt*2
    print '%i muons are associated to a single DT segment'%d.histo['single-segment muons']['all']
    print 'Of these, %i of them did not have any other "special" feature \nthat could explain the "promotion" from segment to muon'%d.histo['single-segment muons']['has_something_else'][False]

  
  if 'total' in d.histo['reps']['sameChamber']:
    print '\n'+septxt*2
    print '%i muons have 2 segments from the same chamber'%d.histo['reps']['sameChamber']['total']
    if 'total' in d.histo['reps']['identical']:
      print '- %i of these have 2 segments that are identical, (they are duplicated in the segments list)'%d.histo['reps']['identical']['total']
    if 'not identical; by not-repeated-digis num' in d.histo['reps']:
      print '- %i of these differ in the hits, and thus de fitting result'%d.histo['reps']['not identical; by not-repeated-digis num']['total']
      print '  this is the distribution of number of differing digis from the two same-chamber segments'
      print '  ', {k:v for k,v in d.histo['reps']['not identical; by not-repeated-digis num'].items() if k!='total'}
      print '  the "0" entry in the histogram means no different digis: this may be because'
      print '  the digis are the same but for some of the digis, a different laterality is chosen'

  if 'wild_t0_mu' in d.histo:
    print '\n'+septxt*2
    
    print '%i of the muons have a big discrepancy between the hits\' expected and actual postions'%d.histo['wild_t0_mu']
    print '(sum((expected_pos - pos)^2) > %.4f cm^2):'%SUM_POS_ERR_SQ_THR
    print '- the expected postions are calculated from the fitting pos & slope, which is calculated from fitting the hits'
    print '- the actual postions are calculated by sum of wirePos +- drift_dist,'
    print '  where the drift_dist is calculated with the T0 of the event, not the T0 of the fit'
    print 'so, a discrepancy between those numbers indicate that the segment was fitted and its t0'
    print 'did not belong to the current event, but nonetheless it was used to build a muon with it'
    print
    print 'This is some data on the worst of these muons'
    print '- it has %i segments'%d.worst_mu['mu']['staMu_nMatchSeg']
    print '- the fitted T0s in ns for its (phi) segments are (event t0 = 0):', map(lambda x:round(x,1),d.worst_mu['phi_t0s'])
    print
    print 'This is some data on the one that has more distant t0s'
    print '- it has %i segments'%d.traveller_mu['mu']['staMu_nMatchSeg']
    print '- the fitted T0s in ns for its (phi) segments are (event t0 = 0):', map(lambda x:round(x,1),d.traveller_mu['phi_t0s'])
    print
    if -999. in d.worst_mu['phi_t0s'] or -999. in d.traveller_mu['phi_t0s']:
      print ' NOTE: "-999.0" indicates that a particular segment was not fit (maybe all lateralities were equal)'
    
    if raw_input('\nDo you want to print the complete segment info for these muons? Type y for yes, any other to continue: ') == 'y':
      pprint(d.worst_mu['segs'])
      print '-'*50
      pprint(d.traveller_mu['segs'])
      
    
  print '\n'+septxt*2
  
  if raw_input('\nDo you want to print the complete data structure histogram with all the data? Type y for yes, any other to continue: ') == 'y':
    pprint(d.histo)



#########################################
#########################################
if __name__ == '__main__':
  run(sys.argv, d, process, finish)

