#!/bin/env python
import sys
from misc_utils import AttrDict, histo_add
from dttree_utils import prefetchFields, byWhSeSt
from runanalysis import run
import ROOT
ROOT.gROOT.SetBatch(True) # Prevent pop-up plots


### Define the plots and analysis variables
d = AttrDict({
  'plots' : {
    'timebox'           : ROOT.TH1D('timebox',"Timebox of YB2 S12 MB1",2000,0,2000),
    'NhitsEvent'        : ROOT.TH1D('NhitsEvent',"Number of hits per event",100,0,100),
    'NhitsEventin16BXs' : ROOT.TH1D('NhitsEventin16BXs',"Number of hits per event in a 16 BXs window",100,0,100),
    },

  'nSeg' : 0,
  
  })

### This implementation accesses the NTuple directly
def process(entry, d, i_entry):
  d.nSeg += entry.ph2Seg_nSegments
  
  Nhits = 0
  Nhitsin16BXs = 0 
  
  for i_digi in range(entry.ph2Digi_nDigis):
    if (entry.ph2Digi_sector[i_digi]==12 and entry.ph2Digi_wheel[i_digi]==2 and entry.ph2Digi_station[i_digi]==1):
      t = entry.ph2Digi_time[i_digi]
      Nhits+=1
      if (t > 360 and t < 840): Nhitsin16BXs+=1
      d.plots['timebox'].Fill(t)
  
  d.plots['NhitsEvent'].Fill(Nhits)
  d.plots['NhitsEventin16BXs'].Fill(Nhitsin16BXs)

### This implementation converts each event to a dict first, which is a lot slower
def process(entry, d, i_entry):
  evt = prefetchFields(entry, [
    'ph2Digi_wheel',
    'ph2Digi_sector',
    'ph2Digi_station',
    'ph2Digi_time',
    'ph2Seg_nSegments'
    ])
  
  d.nSeg += evt['ph2Seg_nSegments']
  
  Nhits = 0
  Nhitsin16BXs = 0 
  
  digisByWhSeSt = byWhSeSt(evt['ph2Digi'])
  
  for digi in digisByWhSeSt[(2,12,1)]:
    Nhits+=1
    d.plots['timebox'].Fill(digi['time'])
    if (digi['time'] > 360 and digi['time'] < 840):
      Nhitsin16BXs+=1
  
  d.plots['NhitsEvent'].Fill(Nhits)
  d.plots['NhitsEventin16BXs'].Fill(Nhitsin16BXs)

def finish(d):
  print 'Total number of segments in sample', d.nSeg





#########################################
#########################################
if __name__ == '__main__':
  run(sys.argv, d, process, finish)

