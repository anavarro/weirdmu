import math

# Class AttrDict inherits from dict and allows to access attrDictObject[key] as attrDictObject.key
class AttrDict(dict):
  def __init__(self, *args, **kwargs):
    super(AttrDict, self).__init__(*args, **kwargs)
    self.__dict__ = self

def histo_add(histo, key, value = 1):
  """takes a dict object <histo> and adds <value> to the bin <key>, creating it if it doesn't exist"""
  path = key if isinstance(key,list) else [key]
  for key in path[:-1]:
    if key not in histo: histo[key] = {}
    histo = histo[key]
  key = path[-1]
  if key not in histo:
    histo[key] = [] if isinstance(value,list) else 0
  histo[key] += value

def statistics(data, ndigits=1000):
  """Returns a dict with statistics on a list of numerical values"""
  
  mean = sum(data)/len(data)
  stdev = math.sqrt(sum([(x-mean)**2 for x in data])/len(data))
  
  return {
    'mean': round(mean,ndigits),
    'stdev':round(stdev,ndigits),
    }
